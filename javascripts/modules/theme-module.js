AppName.Modules.ThemeModule = (function() {
  //Dependencies
  var core = AppName.Core;

  // header
  var _stickyNavBar = function() {
    $(window).scroll(function(){
      var sticky = $('.header'),
          scroll = $(window).scrollTop();

      if (scroll >= 100) sticky.addClass('sticky-navbar');
      else sticky.removeClass('sticky-navbar');
    });
  }

  // home page
  var _heroBanner = function(){
    $(document).ready(function() {
      setInterval(function() {
        var widnowHeight = $(window).height();
        var containerHeight = $(".padding-top-bottom-auto").height();
        var padTop = widnowHeight - containerHeight;
        $(".padding-top-bottom-auto").css({
            'padding-top': Math.round(padTop / 2) + 'px',
            'padding-bottom': Math.round(padTop / 2) + 'px'
        });
      }, 10)
    });
  }

  var _matrix = function(){
    // geting canvas by id c
    var c = document.getElementById("c");
    var ctx = c.getContext("2d");

    //making the canvas full screen
    c.height = window.innerHeight;
    c.width = window.innerWidth;

    //chinese characters - taken from the unicode charset
    var matrix = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789@#$%^&*()*&^%";
    //converting the string into an array of single characters
    matrix = matrix.split("");

    var font_size = 10;
    var columns = c.width/font_size; //number of columns for the rain
    //an array of drops - one per column
    var drops = [];
    //x below is the x coordinate
    //1 = y co-ordinate of the drop(same for every drop initially)
    for(var x = 0; x < columns; x++)
        drops[x] = 1;

    //drawing the characters
    function draw()
    {
        //Black BG for the canvas
        //translucent BG to show trail
        ctx.fillStyle = "rgba(0, 0, 0, 0.04)";
        ctx.fillRect(0, 0, c.width, c.height);

        ctx.fillStyle = "#0F0"; //green text
        ctx.font = font_size + "px arial";
        //looping over drops
        for(var i = 0; i < drops.length; i++)
        {
            //a random chinese character to print
            var text = matrix[Math.floor(Math.random()*matrix.length)];
            //x = i*font_size, y = value of drops[i]*font_size
            ctx.fillText(text, i*font_size, drops[i]*font_size);

            //sending the drop back to the top randomly after it has crossed the screen
            //adding a randomness to the reset to make the drops scattered on the Y axis
            if(drops[i]*font_size > c.height && Math.random() > 0.975)
                drops[i] = 0;

            //incrementing Y coordinate
            drops[i]++;
        }
    }

    setInterval(draw, 35);

  };

  // etc
  var _pageLoading = function() {
    $(window).bind("load", function() {
        $(".page-loading").fadeOut(1000);
    });
  }

  /////////////////////
  // Public Methods //
  ///////////////////
  var init = function() {
    // header
    _stickyNavBar();

    // home page
    _heroBanner();
    if ($('.homepage, .about').length > 0) {
      // _matrix();
    }

    // etc
    _pageLoading();
  };

  return {
    init: init
  };
})();
